function Stegano(input) {
	this.canvas = document.getElementById('out');
	this.capacity = 0;
}

Stegano.prototype.load = function(img) {
	this.canvas.height = img.height;
	this.canvas.width = img.width;

	this.ctx = this.canvas.getContext('2d');

	this.ctx.drawImage(img, 0, 0);

	this.data = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);

	this.capacity = this.data.data.length / 8;
}

Stegano.prototype.storeData = function(str) {
	for (var charpos = 0; charpos < str.length; charpos++) {
		var c = str.charCodeAt(charpos);
		for (var bitpos = 0; bitpos < 8; bitpos++) {
			var posInImgData = (charpos * 8 + bitpos) + ~~((charpos * 8 + bitpos + 1) / 3) + 1;

			var bit = ((c >> bitpos) & 1);

			var currentValue = this.data.data[posInImgData];

			if (currentValue % 2 == 1 && bit == 0 || currentValue % 2 == 0 && bit == 1) {
				if (currentValue == 255) {
					currentValue--;
				} else {
					currentValue++;
				}
			}

			this.data.data[posInImgData] = currentValue;
		}
	}

	this.ctx.putImageData(this.data,0 , 0);
}

Stegano.prototype.readData = function(length) {
		var str = '';

		if (!length) length = this.capacity;

		for (var charpos = 0; charpos < length / 8; charpos++) {
			var c = 0;
			for (var bitpos = 0; bitpos < 8; bitpos++) {
				var posInImgData = (charpos * 8 + bitpos) + ~~((charpos * 8 + bitpos + 1) / 3) + 1;
				if (!(this.data.data[posInImgData] % 2 == 0)) {
					c = c | (1 << bitpos);
				}
			};
			str += String.fromCharCode(c);
		};
		return str;
}

Stegano.prototype.handleFileSelect = function(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	if (evt.dataTransfer && evt.dataTransfer.files) {
		var files = evt.dataTransfer.files;
	} else if (evt.target && evt.target.files) {
		var files = evt.target.files;
	}
	for (var i = 0; i < files.length; i++) {
		var t = t;
		var f = files[i];
		var fr = new FileReader();
		fr.file = f;
		fr.onload = function(e) {
			var img = new Image();
			img.onload = function() {
				window.s.load(this);
			}
			img.src = e.target.result;
		};
		fr.readAsDataURL(f);
	}
}


function test() {
	var str = '';
	var append = 'test Test test Test';
	while (str.length < s.capacity + append.length) {
		str += append;
	}
	s.storeData(str);
}

function test2() {

	for (var i = 0; i < 100; i++) {
		console.log(i, i + ~~((i + 1) / 3) +1)
	}
}

window.s = new Stegano();
document.getElementById('input').onchange = window.s.handleFileSelect;
